import os

from tflearn import local_response_normalization

from pathos.constants import *
from matplotlib import pyplot as plt

import tflearn
import tflearn.layers as tflayers
from pathos import loader

# The fer-2013 dataset has only 48x48 sized images
SAVE_DIRECTORY = "./saved_data/"


class EmotionRecognitionModel:

    def __init__(self, name):
        self._name = name
        self.target_classes = EMOTIONS
        self.target_classes_macros = ['ANG', 'DIS', 'FEA', 'HAP', 'SAD', 'SUR', 'NEU']
        self.datasetLoader = loader.DatasetLoader()

    def build_network(self):
        print("[INFO] Building neural network")

        # Start off with grayscale 48x48 input data
        self.network = tflayers.core.input_data(shape=[None, INPUT_SIZE, INPUT_SIZE, 1])

        # Convolution
        self.conv0 = tflayers.conv.conv_2d(self.network, 64, 5, activation='relu')
        # self.network = local_response_normalization(self.network)
        self.max_pool1 = tflayers.conv.max_pool_2d(self.conv0, 3, strides=2)
        self.conv1 = tflayers.conv.conv_2d(self.max_pool1, 64, 5, activation='relu')
        self.max_pool2 = tflayers.conv.max_pool_2d(self.conv1, 3, strides=2)
        self.conv2 = tflayers.conv.conv_2d(self.max_pool2, 128, 4, activation='relu')

        # The fully connected layers
        self.network = tflayers.core.dropout(self.conv2, 0.3)
        self.network = tflayers.core.fully_connected(
            self.network,
            3072,
            activation='relu'
        )
        self.network = tflayers.core.fully_connected(
            self.network,
            len(self.target_classes),
            activation='softmax'
        )
        self.network = tflayers.estimator.regression(
            self.network,
            optimizer='momentum',
            loss='categorical_crossentropy'
        )

        self.model = tflearn.DNN(
            self.network,
            checkpoint_path=os.path.join(SAVE_DIRECTORY, self._name),
        )

        observed = [self.max_pool1, self.max_pool2, self.conv0, self.conv1, self.conv2];
        self.observers = [tflearn.DNN(v) for v in observed]
        
        self.load_model()
        
        print("[INFO] Finished building neural network")

    def load_saved_dataset(self):
        self.datasetLoader.load_from_save()
        print("[INFO] Dataset found and loaded.")

    def save_model(self):
        self.model.save(os.path.join(SAVE_DIRECTORY, self._name))
        print("[INFO] Model saved.")

    def load_model(self):
        print("looking for network in " + os.path.join(SAVE_DIRECTORY, self._name + ".meta"))
        if os.path.isfile(os.path.join(SAVE_DIRECTORY, self._name + ".meta")):
            self.model.load(os.path.join(SAVE_DIRECTORY, self._name))
            print("[INFO] Model loaded from filesystem.")

    def predict(self, image):
        import numpy as np
        import cv2
        if image is None:
            return None
        image = image.reshape([-1, 48, 48, 1])

        outputs = [m.predict(image) for m in self.observers]
        print([d.shape for d in outputs])
        #output = outputs[0][0,:,:,:].reshape([-1, 24, 24*64, 1])
        print(outputs[4].shape)
        output = outputs[2]
        #print(output.reshape([-1, 24, 24, 1]).shape)
        #output = output.reshape([-1, output.shape[1], output.shape[2]*output.shape[3], 1])
        #for i in np.arange(0, output.shape[3]):
            #plt.subplot(8, 8, i + 1)
            #plt.pcolor(np.flipud(output[0,:,:,i]), cmap='hot')
        output = cv2.cvtColor(output[0,:,:,3], cv2.COLOR_GRAY2BGR)

        cv2.imshow('hotmap', output)

            #plt.axis('off')
       # plt.show()
        #print([d.shape for d in outputs])
        
        return self.model.predict(image)

    def train(self):
        self.load_saved_dataset()
        self.build_network()
        if self.datasetLoader is None:
            self.load_saved_dataset()
        self.load_model()
        # Training
        
        print('[+] Training network')
        self.model.fit(
            self.datasetLoader.images, self.datasetLoader.labels,
            validation_set=(self.datasetLoader.images_test,
                            self.datasetLoader.labels_test),
            n_epoch=100,
            batch_size=50,
            shuffle=True,
            show_metric=True,
            snapshot_step=200000000000,
            snapshot_epoch=True,
            run_id='emotion_recognition'
        )

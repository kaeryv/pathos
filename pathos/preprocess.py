#!/bin/python3

import pandas as pd
import json
import os
import argparse

# Computer vision
import cv2
import dlib
import numpy as np



def detect_faces(detector, raw_image):
    # Convert the image to grayscale
    gray = cv2.cvtColor(raw_image, cv2.COLOR_BGR2GRAY)
    
    ''' 
    Method to artificially enlarge the picture, in order to secure face contouring.
    This method is computationally expensive, raising all images' resolution.
    '''
    
    
    #height, width = gray.shape
    #gray_border = np.zeros((3*height, 3*width), np.uint8)
    #gray_border[:, :] = 200
    #gray_border[
    #    int((3*height / 2) - (height / 2)): int((3*height / 2) + (height / 2)),
    #    int((3*width / 2) - (width / 2)): int((3*width / 2) + (width / 2))
    #] = gray
   
    # We use dlib's face detector
    rects = detector(gray, 1)
    
    faces = list()

    for (i, rect) in enumerate(rects):
        x1, x2, y1, y2 = rect.tl_corner().x, rect.br_corner().x, rect.tl_corner().y, rect.br_corner().y
        focused_face_gray = gray[y1:y2, x1:x2]
        faces.append(focused_face_gray)
    return faces

def g48_standardize(gray_image):
    # g48 stands for 48x48 grayscale image
    # gray_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2GRAY)
    gray_image = cv2.equalizeHist(gray_image)

    try:
        gray_image = cv2.resize(gray_image, (48, 48), interpolation=cv2.INTER_CUBIC) / 255.
    except Exception:
        print("[Error] While resizing.")
        return None
    return gray_image



if __name__ == '__main__':
    print('starting preprocess.')
    
    parser = argparse.ArgumentParser(description='Annotates media with 7 emotions weights.')
    parser.add_argument('--training-database', dest='training', action='store_true')
    parser.add_argument('--validation-database', dest='validation', action='store_true')
    parser.add_argument('--overwrite', action='store_true')
    parser.add_argument('--starting-index', dest='start_at')
    args = parser.parse_args()


    overwrite_mode = args.overwrite
    if args.start_at is not None:
        start_at = args.start_at
    else:
        start_at = 0
    
    hog_fd = dlib.get_frontal_face_detector()

    # We start by loading the installation configuration
    f = open('./config.json')
    configuration = json.load(f)
    f.close()
    
    database_root = configuration['affectnet']['database_root']
    training_meta = configuration['affectnet']['training_metafile']
    validation_meta = configuration['affectnet']['validation_metafile']
    emotion_map = configuration['affectnet']['emotions_mapping']
    processed_foldername = configuration['affectnet']['processed_foldername'] 
    raw_foldername = configuration['affectnet']['raw_foldername'] 


    output_folder =  os.path.join(database_root, processed_foldername)
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
 
    def process_db(data):
        for index, row in data.iterrows():
            emotion_id = row[6]
            image_path = row[0]
            
            image_abs_path = os.path.join(database_root, raw_foldername)
            image_abs_path = os.path.join(image_abs_path, image_path)

            current_folder = os.path.join(output_folder, os.path.dirname(image_path))

      
            if not index%1000: 
                print('Progress: ' + str(index) + ' images.')
           
            # The file name without the extension
            filename = os.path.basename(image_abs_path)
            filename = os.path.splitext(filename)[0]
            
            # The output file
            processed_filename = os.path.join(current_folder, filename + '.png')

            # In overwrite mode, we overwrite the existing processed image.
            if (os.path.isfile(processed_filename) and not overwrite_mode) or index < start_at:
                continue
            if not os.path.exists(current_folder):
                os.makedirs(current_folder)
 


            image_raw = cv2.imread(image_abs_path)
            
            if image_raw is None:
                print("Unable to open image matrix " + str(image_abs_path))
                continue
            
            faces = detect_faces(hog_fd, image_raw)

            if len(faces) != 0 and faces[0] is not None:
                current_folder = os.path.join(output_folder, os.path.dirname(image_path))

                if not os.path.exists(current_folder):
                    os.makedirs(current_folder)
           
                if not index%1000: 
                    print('Progress: ' + str(index) + ' images.')
               
                # The file name without the extension
                filename = os.path.basename(image_abs_path)
                filename = os.path.splitext(filename)[0]
                
                # The output file
                processed_filename = os.path.join(current_folder, filename + '.png')

                # In overwrite mode, we overwrite the existing processed image.
                if os.path.isfile(processed_filename) and not overwrite_mode:
                    continue
                
                faces[0] = g48_standardize(faces[0])
                if faces[0] is None:
                    continue

                faces[0] = faces[0] * 255
                faces[0] = faces[0].astype('uint8')
                print("Processed " + image_path)
                cv2.imwrite(processed_filename, faces[0])

      
    if args.training:
        data = pd.read_csv(os.path.join(database_root, training_meta))
        process_db(data) 
    if args.validation:
        data = pd.read_csv(os.path.join(database_root, validation_meta))
        process_db(data) 
    
    

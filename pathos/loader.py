from os.path import join
import numpy as np
from pathos.constants import *
from sklearn.model_selection import train_test_split

ancm = 'both'
class DatasetLoader:
    def load_from_save(self):
        # Load numpy database
        print("[INFO] Loading numpy database")
        self._images = np.load(join(SAVE_DIRECTORY, SAVE_DATASET_IMAGES_FILENAME))
        self._images_test = np.load(join(SAVE_DIRECTORY, \
                'affectnet.pictures.'+ancm+'.npy'))
        self._labels = np.load(join(SAVE_DIRECTORY, SAVE_DATASET_LABELS_FILENAME)).reshape([-1, len(EMOTIONS)])
        self._labels_test = np.load(join(SAVE_DIRECTORY, \
                'affectnet.labels.'+ancm+'.npy')).reshape([-1, len(EMOTIONS)])

        self._images = self.images.reshape([-1, SENSOR_SIZE, SENSOR_SIZE, 1])
        self._images_test = self.images_test.reshape([-1, SENSOR_SIZE, SENSOR_SIZE, 1])

        print(self._labels.shape)
        #print(self._images_test.shape)
        # Setting up train framework
        #self._images, self._images_test, self._labels, self._labels_test = train_test_split(self._images, self._labels,
        #                                                                                    test_size=0.20,
        #                                                                                    random_state=42)
        #print(self._labels_test.shape)

        #print(self._images_test.shape)
    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def images_test(self):
        return self._images_test

    @property
    def labels_test(self):
        return self._labels_test
